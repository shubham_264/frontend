import React, { Component } from 'react';
import Toolbar from '../../components/Toolbar/Toolbar';
import SideDrawer from '../../components/sideDrawer/sideDrawer';
import Home from '../../components/Home/Home';
import { BrowserRouter,Route, Switch} from 'react-router-dom';
import Register from '../../components/Register/Register';
import Login from '../../components/Login/Login';
import Footer from '../../components/Footer/Footer';
import FAQ from '../../components/FAQ/FAQ';
import freeTrial from '../../components/freeTrial/freeTrial';
import Contact from '../../components/Contact/contact';

class Layout extends Component {
    state= {
        showSideDrawer: false
    }

    sideDrawerClosedHandler = () => {
        this.setState({showSideDrawer: false});
    }

    sideDrawerToggleHandler = () => {
        this.setState((prevState) => {
            return {showSideDrawer: !prevState.showSideDrawer};
        });
    }
    render () {
        return(
            <div>
              <BrowserRouter>
              
                <Toolbar drawerToggleClicked={this.sideDrawerToggleHandler}/>
                <SideDrawer open={this.state.showSideDrawer}
                closed={this.sideDrawerClosedHandler} />
                 <Route exact path ="/" component = {Home} />
                 <Route exact path ="/register" component = {Register} />
                <Route exact path ="/login" component = {Login} />
                <Route exact path ="/faq" component = {FAQ} />
                <Route exact path ="/freetrial" component = {freeTrial} />
                <Route exact path ="/contact" component = {Contact} />
                <Footer/>
                
                 
                </BrowserRouter>
            </div>
        )
    }
}

export default Layout;