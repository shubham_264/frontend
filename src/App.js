import React from 'react';
import './App.css';
import Navigation from './components/Navigations/Navigation';
import Toolbar from './components/Toolbar/Toolbar';
import 'bootstrap/dist/css/bootstrap.css';
import Layout from './containers/Layout/Layout';
import { BrowserRouter,Route, Switch, withRouter} from 'react-router-dom';
import Home from './components/Home/Home';
import Register from './components/Register/Register';
import Login from './components/Login/Login';
import sideDrawer from './components/sideDrawer/sideDrawer';
import Footer from './components/Footer/Footer';
import FAQ from './components/FAQ/FAQ';


function App() {
  
  return (
    <div>
      
      <Layout/>
      
      
    </div>
  );
}

export default App;
