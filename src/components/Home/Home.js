import React, { Component,img } from 'react';
import ReactDOM from 'react-dom';
import Image from 'react-bootstrap/Image';
import 'bootstrap/dist/css/bootstrap.css';
import home from '../../assests/images/home.webp';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Home.scss';
import { Container, Col, Form, Row, FormGroup, Label, Input } from 'reactstrap'; 
import Button from '../UI/Button/Button';
import div2 from '../../assests/images/div2.jpg';

class Home extends Component {
    state = {  }
    render() { 
        return (  
            <Container>
                 <Row>
          
                <Col lg={5} md={12} className="home">
               
                <h1>eCommerce photo editing done for you</h1>
                <p>Pixc is the easiest way to get your product images edited - without additional resources.</p>
                <Button>Start Free Trial</Button>
                <p style={{fontSize: '10px', color: '#bbb'}}>*Upload your trial photo. 24 hour delivery.</p>
       </Col>
      <Col className="pic1" lg={7} md={12}>
      
      
                <img 
      src="https://images.pexels.com/photos/20787/pexels-photo.jpg?auto=compress&cs=tinysrgb&h=350"
      alt="new"
      
      />  
             </Col>
             </Row>
             <Row>
            <Col lg={2}>

            </Col>
            <Col lg={8} md={12} className="div2" >
            
             <h2>Create stand out product pages with beautiful images</h2>
             <p>Let your products speak for themselves with high quality, professionally edited photographs that have been optimized for any eCommerce platform or marketplace.</p>
             
             
            
             </Col>
             <Col lg={2} md={12}>

            </Col>
             </Row>
             <Row>
                 
                <Col lg={12} md={12} className="image">
             <img src= {div2}
             alt="new"/>
             </Col>
             </Row>
                <hr/>
             <Row>
            {/* <Col lg={2}>

            </Col> */}
            <Col lg={12} md={12}>
             <div className="div2">
             <h2>Integrate, manage and collaborate in one solution</h2>
             <p>Our photo editing platform helps you and your team manage all your product photos within one easy-to-use, intuitive dashboard. We integrate with all major platforms, making managing your visual content a breeze.

            </p>
             
             </div>
            
             </Col>
             {/* <Col lg={2}>

            </Col> */}
             </Row>
             <Row>
                 
                 <Col lg={12} md={12} className="image">
             <img src= {home}
             alt="new"/>
             </Col>
             </Row>
            
             <Row className="div4">
                 <Col lg={2}>

                 </Col>
                 <Col lg={3}>
                     <img  src="https://dashboard-0.pixc.com/assets/images/new/content/front-upload-icon.png" />
                     <h3>Upload</h3>
                     <p>Upload photos to your Pixc dashboard from your computer, Dropbox or online store.

                    </p>
                 </Col>
                 
                 <Col lg={3}>
                     <img src="https://dashboard-0.pixc.com/assets/images/new/content/front-options-icon.png" />
                     <h3>Select Your Option</h3>
                     <p>Choose and customize a template that fits your store’s exact requirements.</p>
                 </Col>
                 <Col lg={3}>
                     <img src="https://dashboard-0.pixc.com/assets/images/new/content/front-download-icon.png"/>
                     <h3>Review</h3>
                     <p>You’ll receive professionally edited photos within 24 hours. Publish them directly to your store.</p>
                 </Col>

             </Row>
             <hr />
             </Container>
             
        );
    }
}
 
export default Home;
