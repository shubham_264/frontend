import React from 'react';
import Logo from '../../assests/images/pixc-logo.svg';
import Laptop from '../../assests/images/laptop.jpg';
import Navigation from '../Navigations/Navigation';
import './Toolbar.scss';
import DrawerToggle from '../sideDrawer/DrawerToggle/DrawerToggle';
import 'bootstrap/dist/css/bootstrap.css';


const toolbar =(props) => (
   
    <div className="container" >
        <header className="Toolbar">
        <DrawerToggle clicked={props.drawerToggleClicked} />
    
            <img src={Logo} />
        
        <nav className="DesktopOnly">
            <Navigation />
        </nav>
    </header>
    </div>
    
) ;

export default toolbar;