import React from 'react';
import { Container, Col, Row,  Form, FormGroup, Label, Input } from 'reactstrap';
import Button from '../UI/Button/Button';
import './Register.scss';
const Register = (props) => {
  return (
    <Container>
     
          
              <Row >
                <Col md={3} >

                </Col>
                <Col md={6}  className="form"  >  
                <h3 className="heading">Get started with 5 FREE credits</h3>
    <Form >
      <Row form>
        <Col md={6}>
          <FormGroup>
            <Label for="firstname">First Name:</Label>
            <Input type="text" name="firstname" id="firstname"  />
          </FormGroup>
        </Col>
        <Col md={6}>
          <FormGroup>
            <Label for="lastname">Last Name:</Label>
            <Input type="text" name="lastname" id="lastname"  />
          </FormGroup>
        </Col>
      </Row>
      <FormGroup>
        <Label for="email">Email:</Label>
        <Input type="email" name="email" id="email" />
      </FormGroup>
      <Row form>
        <Col md={6}>
          <FormGroup>
            <Label for="password">Password:</Label>
            <Input type="password" name="password" id="password"  />
          </FormGroup>
        </Col>
        <Col md={6}>
          <FormGroup>
            <Label for="confirm password">Confirm Password:</Label>
            <Input type="password" name="confirmpassword" id="confirmpassword"  />
          </FormGroup>
        </Col>
      </Row>
     
      <Button >Continue</Button>
    </Form>
    </Col>
    <Col md={3} >
      <div className="link">
    
     Already Registered?
    <a style={{paddingLeft:'15px', color:'black'}} href="/">Log In</a>
    </div>
                </Col>
     
    </Row>
    

   
    
    
    </Container>
  );
}

export default Register;