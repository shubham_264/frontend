import React, { Component } from 'react'
import { Accordion, Card } from 'react-bootstrap'

export default class FAQ extends Component {
    
    render() {
        return (
            <div>
                <h1>Your FAQs answered.</h1>
                <Accordion >
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="0">
                            What's the turnaround time?
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="0">
                        <Card.Body> All images are returned to you within 24 hours from the time you submitted a paid order. If in case your orders will be delayed due to unforeseen circumstances, our customer success team will reach out to you.</Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="1">
                            2
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="1">
                        <Card.Body> 2 body</Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="2">
                            2
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="2">
                        <Card.Body> 2 body</Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="3">
                            2
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="3">
                        <Card.Body> 2 body</Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="4">
                            2
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="4">
                        <Card.Body> 2 body</Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="5">
                            2
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="5">
                        <Card.Body> 2 body</Card.Body>
                        </Accordion.Collapse>
                    </Card>
                </Accordion>
            </div>
        )
    }
}
