import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {Link, BrowserRouter, Route, Switch} from 'react-router-dom';
import './navigation.scss';
import Button from '../UI/Button/Button';
import Home from '../Home/Home';
import Register from '../Register/Register';


class Navigation extends Component {
    render() {
        return (
               
                   
                        <ul className="navigation">
                            
                            <li>
                                <Link to="/freetrial" >Free Trial</Link>
                            </li>
                            <li>
                                <Link to="/" >Pricing</Link>
                            </li>
                            <li>
                                <Link to="/" >Gallery</Link>
                            </li>
                            <li>
                                <Link to="/" >Testimonials</Link>
                            </li>
                            <li>
                                <Link to="/" >Photography Guides</Link>
                            </li>
                           
                            <li>
                                <Link to="/login" >Log In</Link>
                            </li>
                            <li>
                                <Link to="/register">
                                <Button >Get Started</Button>
                                </Link>
                            </li>
                        </ul>
                       
                       
                  
                
                
            
        )
    }
}

export default Navigation;
