import React, { Component } from 'react';
import {Container, Col, Row} from 'reactstrap';
import './contact.scss';
import Button from '../UI/Button/Button';
import './contact.scss';

export default class contact extends Component {
    render() {
        return (
            <div>
                <Container>
                   <div className="cont1">
                       <h1>Say hello</h1>
                       <p>Our support team is here to help answer your questions 24/7.</p>
                   </div>
                   <hr />
                   <Row>
                       <Col lg={2}  className="contact">
                            <h6>Help Center</h6>
                            <hr/>
                            <p>Find help and assistance via our help center.</p><br/>
                            <Button>Help Center</Button>
                       </Col>
                       <Col lg={2} className="contact">
                            <h6>Chat Now</h6>
                            <hr/>
                            <p>Talk to one of our customer success representatives to see how we can help.</p>
                            <Button>Live Chat</Button>
                       </Col>
                       <Col lg={2} className="contact">
                            <h6>Email</h6>
                            <hr/>
                            <p>Send an email to hello@pixc.com</p><br/><br/>
                            <Button>Email</Button>
                       </Col>
                       <Col lg={2} className="contact">
                            <h6>Contact Form</h6>
                            <hr/>
                            <p>Fill out our short online form.</p><br/><br/>
                            <Button>Contact Form</Button>
                       </Col>
                   </Row>
                </Container>
            </div>
        )
    }
}
