import React, {Component}from 'react'
import {Container, Col, Row } from 'reactstrap';
import Trial from '../../assests/images/trial.png';
import './freeTrial.scss';
import Button from '../UI/Button/Button';
import 'font-awesome/css/font-awesome.min.css';



export default class FreeTrial extends Component {
    
    render()
 {    
    return (
        <Container>
        <Row>
            <Col lg={6} className="trialcont">
                <h1>
                See how good it is for yourself
                </h1>
                <p>
                Pixc supports thousands of online stores worldwide achieve professionally edited photos. Let us help you too!
                </p>
            </Col>
            <Col lg={6} className="trialimg">
                <img src={Trial} />
            </Col>
        </Row>
        <Row>
           
            <Col className="trialcont2">
                <h2>Upload your free trial image</h2>
                <p>
                It’s simple as 1, 2, 3. <br />
                Upload your image and we will show you what we can do.


                </p>
            </Col>
           
        </Row>
        <hr/>
        <Row>
            <Col lg={3}>
            </Col>
            <Col className="trialcont3">
            
            
            <h2>Step 1/3 Upload an image</h2>
            <p>Select an image and we will have it edited for you within 24 hours.</p>
            <div className="upload">
            <i className="fa fa-cloud-upload fa-3x" aria-hidden="true"></i>
                <p>Drag & Drop file here or click to browse.</p>
                <Button>Browse</Button>
            </div>
            </Col>
            <Col lg={3}>
            </Col>
        </Row>
        <hr/>
        </Container>
    )
}
}
