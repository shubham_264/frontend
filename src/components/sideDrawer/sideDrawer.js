import React from 'react';
import Logo from '../../assests/images/pixc-logo.svg';
import Navigation from '../Navigations/Navigation';
import Backdrop from '../UI/Backdrop/Backdrop';
import './sideDrawer.scss';

const sideDrawer = (props) => {
    let attachedClasses = ['SideDrawer', 'Close'];
    if(props.open){
        attachedClasses = ['SideDrawer', 'Open'];
    }
    return (
        <div>
            <Backdrop show={props.open} clicked={props.closed} />
            <div className={attachedClasses.join(' ')} onClick={props.closed}>
                
                <nav>
                    <Navigation />
                </nav>

            </div>
        </div>
    );
};

export default sideDrawer;