import React, { Component } from 'react';
import './Footer.scss';
import {Container ,Col,Row} from 'reactstrap';
import 'font-awesome/css/font-awesome.min.css';

export default class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <Container>
                <Row>
                    
                    <Col lg={3}>
                <ul>
                    <li><a href="/">Pics Extra</a></li>
                    <li><a href="/faq">FAQs</a></li>
                    <li><a href="/">Public API</a></li>
                    <li><a href="/">Affiliate Program</a></li>
                </ul>
                </Col>
                <Col lg={5}>
                </Col>
                <Col lg={3}>
                <ul>
                    <li>PIXC</li>
                    <li><a href="/contact"> Contact</a></li>
                </ul>
                </Col>
                
                </Row>
                <hr/>
                <Row>
                    
                    <Col style={{color:'white', paddingLeft:'5%'}} >
                    2020 © Pixc. All Rights Reserved. | <a style={{color:'white'}} href="/">Legal</a>
                    </Col>
                    <Col className="icon" lg={4}>
                    <a href="/" className="fa fa-facebook fa-2x" aria-hidden="true"></a>
                    <a href="/" className="fa fa-twitter fa-2x" aria-hidden="true"></a>
                    <a href="/" className="fa fa-instagram fa-2x" aria-hidden="true"></a>
                    <a href="/" className="fa fa-pinterest fa-2x" aria-hidden="true"></a>
                    <a href="/" className="fa fa-linkedin fa-2x" aria-hidden="true"></a>
                    </Col>
                </Row>
                </Container>
            </div>
        )
    }
}
