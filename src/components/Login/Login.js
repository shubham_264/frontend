import React, { Component } from 'react';
import {Col, Row,  Form, FormGroup, Label, Input, Button } from 'reactstrap';
import laptop from '../../assests/images/laptop.jpg';
import './Login.scss';
import { FacebookLoginButton } from "react-social-login-buttons";
import { GoogleLoginButton } from "react-social-login-buttons";
import { LinkedInLoginButton } from "react-social-login-buttons";



export default class Login extends Component {
    render() {
        return (
            <div >
                <Row>
                    <Col lg={3} md={12} className="cont">
                        <img src={laptop} />
                        <h6>“Coming together is a beginning; keeping together is progress; working together is success.”  - Henry Ford
                                </h6>
                               
                               
                    </Col>
                     <Col lg={2} md={12}>
                    </Col>
                    <Col lg={6} md={12} >
                    <div className="account">
                        <p>Don't have an account?&nbsp;&nbsp;&nbsp;</p>
                        <Button size="sm" style={{backgroundColor:'#665cc7', padding:'10px', marginTop:'-10px'}}>Get Started</Button>
                    </div>
                    <div className="form">
                        <h3>Welcome back to Netcut</h3>
                       <h6>Enter your details below to login</h6>
                        <Form>
                        <FormGroup>
                            <Label for="email">Email:</Label>
                            <Input type="email" name="email" id="email" placeholder="Email" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="email">Password:</Label>
                            <Input type="password" name="password" id="password" placeholder="************" />
                        </FormGroup>
                        <FormGroup check>
                            <Label check>
                            <Input type="checkbox" />{' '}
                            Keep me signed in
                            </Label>
                        </FormGroup>
                       
                            <Button style={{margin: '10px 0'}}  color="danger" size="lg" block>Log in</Button>
                            
                        </Form>
                        <p style={{textAlign: 'center'}}>Or sign up with:</p>
                        <Row>
                            <Col lg={4} md={12}>
                            <GoogleLoginButton className="button">Google</GoogleLoginButton>
                            </Col>
                            <Col lg={4} md={12}>
                        <FacebookLoginButton className="button">Facebook</FacebookLoginButton>
                        </Col>
                        <Col lg={4} md={12}>
                        <LinkedInLoginButton >LinkedIn</LinkedInLoginButton>
                        </Col>
                        </Row>
                        </div>
                    </Col>
                    <Col lg={3} md={12}>
                   
                    </Col>
                </Row>
            </div>
            
        )
    }
}
