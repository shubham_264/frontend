import React, { Component } from 'react';
import './Button.scss';

const button = (props) => (
    <button className="button">{props.children}  </button>
);

export default button;
